# German jekyll introduction for pupils

This is a set of small and relatively self contained lessons which accompany courses I teach voluntary at schools or as part of the [Hacker School](http://www.hacker-school.de/) initiative.
