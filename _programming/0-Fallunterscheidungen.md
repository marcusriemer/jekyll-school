---
layout: post
author: Marcus
title: Fallunterscheidungen
test: false
description: >
  Nicht immer sollen oder können im Frontmatter alle Daten angegeben werden. Um trotzdem eine sinnvolle Seite anzubieten müssen wir auf Basis der vorhandenen Informationen die Seite unterschiedlich darstellen können.
---

{{ page.description }}

Lässt man den Schlüssel `author` im Frontmatter einer beliebigen Seite einfach leer geschieht etwas unpraktisches: Das Trennzeichen zwischen Datum und dem Namen des Autors (ein • sofern nicht von euch verändert) erscheint auch dann, wenn der Autor garnicht bekannt ist. Das liegt daran, dass Jekyll fehlende Schlüssel einfach durch einen sog. "leeren String" abbildet. Dabei handet es sich um eine Abfolge an Zeichen, nur dass die Anzahl dieser Zeichen halt 0 ist. Möchte man den Autoren (samt dem Punkt) nur dann anzeigen, wenn er auch gesetzt worden ist, verwendet man dafür die sog. <code>if</code> Anweisung.

    {%raw%}{% if page.author %}
      • {{ page.author }}
    {% endif %}{%endraw%}

Alle "Programmieranweisungen" der von Jekyll verwendeten Sprache Liquid werden durch Bereiche gekennzeichnet. Anders als bei HTML Elementen wird der Name des Bereichs nicht durch spitze Klammern, sondern durch `{%raw%}{% name %}{%endraw%}` bis `{%raw%}{% endname %}{%endraw%}` gekennzeichnet. Je nach verwendeter Anweisung wird der umschlossene Bereich dann unterschiedlich behandelt.

Im Falle der <code>if</code> Anweisung wird der umschlossene Bereich nur dargestellt, wenn eine irgendwie geartete Form von Vergleich existiert. Steht nach dem <code>if</code> nur der Name eines YAML Schlüssels, kann man den Code wie folgt lesen: Wenn der angegebene Schlüssel existiert, soll der vorhandene Bereich dargestellt werden. Das obige Beispiel gibt also den Namen des Autors **und** den Punkt nur aus, wenn der Schlüssel `author` der Kategorie `page` existiert.

### Der `else` Bereich

Nehmen wir mal an, wir möchten im Falle eines unbekannten Autors den Wert *unbekannt* ausgeben. Dazu verwenden wir die sog. <code>else</code> Anweisung, deren Bereich immer dann dargestellt wird, wenn die Bereich der <code>if</code> Anweisung **nicht** dargestellt wird.

    {%raw%}{% if page.author %}
      • {{ page.author }}
    {% else %}
      • <i>Autor unbekannt</i>
    {% endif %}{%endraw%}

### Die speziellen Werte `true` und `false`

Manchmal möchte man bestimmte Eigenschaften einer Seite nur ein- oder ausschalten, aber keine weiteren speziellen Eigenschaften vergeben. Für diese Fälle verwendet man die Werte `true` und `false` (engl. Wahr und Falsch).

    {%raw%}
    ---
    test: false
    ---
    {%if page.test %}
      Wert ist bestimmt true: {{page.test}}
    {%else%}
      Wert ist bestimmt false: {{page.test}}
    {%endif%}
    {%endraw%}

<div class="task">
    <div class="header">Aufgabe: Optionale Autorennamen und eine weitere Eigenschaft</div>
    <ul>
        <li>Sorgt auch in eurem Template für die Beiträge dafür, dass der Autorenname optional wird. </li>
        <li>Räumt den Beispielcode auf: Da der Punkt in beiden Bereichen erscheint, kann er auch nur einmal vor die Fallunterscheidung geschrieben werden.</li>
        <li>Fügt ein eigenes Attribut hinzu, dass z.B. den Inhalt des ganzen Beitrages ausblendet oder fett druckt, sofern der Wert auf <code>true</code> steht.</li>
    </ul>
</div>

### Vergleiche

Mit den logischen Werten `true` und `false` zu arbeiten ist nicht immer ausreichend, manchmal möchte man Dinge vergleichen. Wir wollen unser Bewertungssystem nun um drei Spezielle Ränge von Bewertern ergänzen. Bewerter auf dem Rang 0 sind wenig vertrauenswürdig, da über sie nicht viel bekannt ist. Bewerter mit Rang 1 haben in ihrem Profil einen Twitter oder Facebook Account hinterlegt, sind also etwas weniger anonym. Bewerter auf Rang 2 sind Kunden der Webseite, von denen die Adresse bekannt ist.

Der Betreiber der Seite möchte nun nicht jedes Produkt von jedem Kunden bewerten lassen. Einige Produkte ziehen möglicherweise Spaßbewertungen an, die ja nicht in allen Fällen erwünscht sind, und sollen deswegen nur von den "wichtigen" Bewertern rezensiert werden können. Wir haben es in diesem Beispiel also mit zwei verschiedenen Werten zu tun: Der Stufe des Bewerters und der Stufenfreigabe des Produktes.

    {%raw%}
    ---
    bewerter:
      stufe: 1
    produkt:
      stufe: 2
    ---
    {% if bewerter.stufe >= produkt.stufe %}
      Bewertung erlaubt
    {% else %}
      Bewertung verboten
    {%endif%}
    {%endraw%}
