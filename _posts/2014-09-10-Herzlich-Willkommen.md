---
layout: post
title: Herzlich Willkommen
author: Marcus
---
Ihr seid anscheinend alle dabei beim LEBL-Kurs "Wir entwickeln einen Blog".

## Themen dieses Kurses

### Einführung
- Bestandteile eines Blogs
- Warum keinen fertigen Blog?
- Die Kommandozeile
- Ein Überblick über Jekyll
  - Konfiguration und Metadaten
  - Schreiben von eigenen Beiträgen
  - Was ist eigentlich Markdown?
- Eine erste Veröffentlichung

### Ein schöneres Layout für unseren Blog
- Ein kurzer Blick auf HTML
  - Zurück auf die Hauptseite
- Der Umgang mit Bootstrap
- Grundlegendes CSS

### Besser als USB Sticks: Versionsverwaltung mit Git
- Was ist Versionskontrolle?
- Der Umgang mit git
  - Mit der Kommandozeile
  - Mit SourceTree

### Eine Fotogalerie
- Übersicht über die benötigten HTML Konstrukte
- Erstellung von Vorschaubildern

### Eine Idee von euch?