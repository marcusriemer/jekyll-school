---
layout: post
author: Marcus
title: Was ist ein Blog?
---

### Bekannte Blogsysteme

- Wordpress, wahlweise selber betrieben oder werbefinanziert auf z.B. <a href="http://www.wordpress.com/">Wordpress.com</a>
- <a href="http://www.blogger.com">Blogger.com</a>, eine von Google betriebene Plattform.
- <a href="http://www.tumblr.com">Tumblr</a>, eine Plattform auf der besonders häufig Bilder publiziert werden.

### Bestandteile eines Blogs

- Beiträge, die in mehr oder minder regelmäßigen Abständen neu verfasst werden.
- Spezielle Seiten, wie z.B. "Über Mich".
- Ein RSS Feed, der interessierte Leser ohne viel Aufwand auf dem Laufenden hält.
- Die Möglichkeit Inhalte des Blogs zu kommentieren.

### Was macht Jekyll anders?

Es werden statische HTML Seiten generiert, anstatt die Seiten dynamisch auf dem Server zu erzeugen.

- Sicher, keine Möglichkeit sich online einzuloggen o.ä.
- Schnell, keinerlei serverseitige Verarbeitung nötig.
- Günstig, wirklich jeder Webhost bietet die Möglichkeit statisches HTML anzubieten.
- Einfach, es muss nur ein Ordner hochgeladen werden und das wars.

Auf der anderen Seite erfordert es Jekyll, das man sich ein bisschen mit den Grundlagen von Webseiten befassen muss. Und genau das machen wir in diesem Kurs.