---
author: Marcus
layout: default
title: Von Jekyll erzeugte Seiten
description: >
  Bisher haben wir die Commandozeile immer nur für den Aufruf <code>jekyll serve --watch</code> benutzt. Jetzt gehen wir einen Schritt weiter und schauen uns an, wie Jekyll uns bei der Veröffentlichung der Webseite unterstützt.
---

{{ page.description }} Praktischerweise müssen wir im einfachsten Fall kaum tätig werden: Auch wenn man Jekyll mit der `serve` Option startet, wird die gesamte Struktur der Seite im Dateisystem hinterlegt. Wenn Ihr einen Blick in den `_site` Ordner werft, seht ihr die komplette von Jekyll erzeugt Struktur. Dieser Ordner kann im Prinzip mit jedem Webserver verwendet werden, um eure Seite in den Weiten des Internets verfügbar zu machen.

### `build` statt `serve`

Wenn man nur den `_site` Ordner erzeugen möchte ohne gleich einen Webserver zu starten kann man einfach das Kommando `jekyll build` verwenden. Dadurch wird der `_site` Ordner einmalig aktualisiert. Außerdem funktioniert dieses Kommando auch, wenn sich der Webserver nicht starten lässt weil z.B. der Benutzer nicht ausreichend Rechte hat um einen Server zu starten.

### Unerwünschte Dateien

Wir haben schon gesehen, dass alle Dateien die mit einem Unterstrich beginnen von Jekyll in irgendeiner Art und Weise speziell behandelt werden. Auf die `develop.cmd` im Ordner der Seite trifft diese Namenskonvention aber nicht zu und trotzdem soll Sie nicht zusammen mit eurer Seite veröffentlicht werden. Um Dateien auszuschließen kann man daher in der `config.yml` eine Liste von Dateinamen angeben, die ignoriert werden sollen:
    
    exclude:
    - develop.cmd

Sollten im Laufe der Zeit also noch mehr Dateien anfallen, die aus irgendwelchen Gründen nicht veröffentlicht werden sollen, können diese einfach in die Liste aufgenommen werden.

### Die Funktion der `baseurl`

Wir hatten im Kapitel [URLs und Jekyll]({{site.baseurl}}/Erste-Schritte/5-URLs-und-Jekyll/) schon gesehen, dass alle URL Angaben (egal ob Bilder, Links, CSS Dateien, ...) immer der Ausdruck `{% raw %}{{ site.baseurl }}{% endraw %}` vorangestellt werden musste. Spätestens beim Veröffentlichen der Seite an der FH Wedel werdet Ihr sehen, warum dieser Schritt zwingend notwendig ist. Habt Ihr die Regel nicht beachtet, werden von Jekyll falsche absolute URLs erzeugt: Es fehlt eure Schülernummer.

### Verschiedene Konfigurationen

Manchmal muss man seine Seite mit verschiedenen Konfigurationen erstellen. Um zum Beispiel diese Anleitungen auf der FH Wedel Seite zu veröffentliche Bedarf es einer speziellen `baseurl`, nämlich `"/~mri/hacker-school"`. Um trotzdem im "normalen" Betrieb ohne dieses Präfix zu arbeiten, legt man einfach eine weitere Konfigurationsdatei an, deren Anweisungen "nach" der Standardkonfiguration verarbeitet werden. In meinem Fall heißt diese Datei `_config_fhw.yml` und hat den folgenden Inhalt:

    destination: _site_fhw
    baseurl: "/~mri/hacker-school" 

Durch einen Aufruf von `jekyll build --config "_config.yml,_config_fhw.yml"` wird dann im Ordner `_site_fhw` (dafür sorgt der Schlüssel `destination`) eine Version der Webseite mit angepasster `baseurl` erzeugt. Theoretisch könnt Ihr der Option `--config` beliebig viele Konfigurationsdateien mitgeben, praktisch sinnvoll sind in fast allen Fällen aber nur exakt zwei Dateien: Erst die Standardkonfiguration, dann die speziellen Werte für das jeweilige Ziel.

<div class="task">
  <div class="header">Aufgabe: Eine Konfiguration für die FH Wedel</div>
  <ul>
    <li>Erzeugt euch eine Konfigurationsdatei mit der korrekten <code>baseurl</code> für eure Schülernummer.</li>
    <li>Ruft Jekyll mit den im Text beschriebenen Parametern auf, um eure Seite in der "Wedelversion" zu erzeugen.</li>
  </ul>
</div>
