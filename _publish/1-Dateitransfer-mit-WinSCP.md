---
author: Marcus
layout: default
title: Dateitransfer mit WinSCP
description: >
  Wie transferiert man seine Webseite vom aktuellen Rechner auf den entsprechenden Server im Internet? Mit WinSCP gibt es ein kostenloses Programm, dass alle gängigen Verfahren zum Dateitransfer beherrscht.
---

{{ page.description }} Den Download gibt es unter [WinSCP.net](http://winscp.net/eng/download.php).

### Verbindungsteinstellungen

Nach dem ersten Start des Programms werdet Ihr von einem Fenster wie diesem begrüßt:

<img class="center" src="{{ site.baseurl }}/assets/winscp-settings.png" />

| Parameter          | Wert                          |
|--------------------|-------------------------------|
| File Protocol      | FTP                           |
|--------------------|-------------------------------|
| Encryption         | TLS Explicit Encryption       |
|--------------------|-------------------------------|
| Hostname           | stud.fh-wedel.de              |
|--------------------|-------------------------------|
| Port               | 21                            |
|--------------------|-------------------------------|
| Benutzer           | `schuelerX`                   |

Das `X` in `schuelerX` müsst Ihr natürlich durch eure Nummer ersetzen. Das Passwort solltet Ihr nach Möglichkeit nicht in WinSCP hinterlegen, sondern jedes Mal eingeben wenn Ihr danach gefragt werdet. Mit dem Knopf "Login" meldet Ihr euch dann schließlich beim Server an.

### Hochladen

Nach der Anmeldung bei dem Server seht Ihr eine zweigeteilte Ansicht: Links euer lokaler Rechner, rechts der entfernte Server. Der eigentliche Dateitransfer ist auf viele Arten möglich, im einfachsten Fall aber per Drag & Drop.

<img class="center" src="{{ site.baseurl }}/assets/winscp-explorer.png" />

### Dateirechte

Nur für den Fall, dass der Webserver mit einer `403 Forbidden` Meldung reagiert an dieser Stelle einmal der Hinweis auf Dateirechte. Es ist möglich dem Webserver mitzuteilen, dass bestimmte Dateien nicht für die Öffentlichkeit gedacht sind. Um diese Dateirechte zu verändern müsst Ihr die Eigenschaften des entsprechenden Ordners auswählen und die Rechte entsprechend der [Hinweisseite mit den RZ-Informationen der FH Wedel](https://www.fh-wedel.de/index.php?id=6473) einstellen. Solltet Ihr die Rechte für den Inhalt nach einem schon getätigten Upload verändern wollen, verwendet Ihr bitte die "Werte rekursiv setzen" Funktion (Checkbox in der letzten Reihe).

<div class="grid">
  <div class="col-1-2">
    <img class="center" src="{{ site.baseurl }}/assets/winscp-permissions-top.png" />
  </div>
  <div class="col-1-2">
    <img class="center" src="{{ site.baseurl }}/assets/winscp-permissions-content.png" />
  </div>
</div>
