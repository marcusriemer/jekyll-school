JEKYLL_BIN = $(shell ruby -e 'puts File.join(Gem.user_dir, "bin", "jekyll")')

http-server:
	$(JEKYLL_BIN) serve --watch

build:
	$(JEKYLL_BIN) build

build-site-fhw:
	$(JEKYLL_BIN) build --config="_config.yml,_config_fhw.yml"

publish-hackerschool.marcusriemer.de:
	rsync _site/. -e "ssh -p 443" -rvz --delete --exclude .git --exclude static/data marcus@ssh.rapunzel.gurxite.de:/srv/htdocs/hackerschool.marcusriemer.de

publish-fh-wedel.de:
	rsync _site/. -e "ssh -p 443" -rvz --delete --exclude .git --exclude static/data marcus@mitserv.fh-wedel.de:/srv/htdocs/hackerschool.marcusriemer.de

.PHONY: http-server build-site-fhw publish-hackerschool.marcusriemer.de
