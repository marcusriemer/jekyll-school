#!/bin/bash

PROJECTS=$(curl http://10.0.4.156:8080/scratch_ids.html | grep ".*:.*" | tr ',' '\n' | tr ':' ',')

# http://wiki.scratch.mit.edu/wiki/Scratch_File_Format_(2.0)

PROJECTS_DIR="assets/scratch/projects"

echo "$PROJECTS" | while read LINE
                   do
                       KEY=$(echo "$LINE" | cut -d "," -f 1)
                       VALUE=$(echo "$LINE" | cut -d "," -f 2 | tr -d '"')
                       if [ "$KEY" -a "$VALUE" ]
                       then
                           echo "$KEY => $VALUE"
                           mv "$PROJECTS_DIR/$VALUE.sb2" "$PROJECTS_DIR/$KEY.sb2"
                       fi
                   done

