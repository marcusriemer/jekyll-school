---
layout: post
author: Marcus
title: CSS Regeln
description: >
  Bisher ist bei der Seite kaum eine Struktur zu erkennen, wir nähern uns den Grundlagen der Gestaltung in dem wir für ein paar Farbtupfer sorgen.
css-files:
- /css/basic-design-0.css
---
{{ page.description }}

### Die erste eigene CSS Datei

CSS Dateien folgen einer immer gleichen Struktur, die auf den ersten Blick sehr viel einfacher ist als die von HTML oder YAML.

{% highlight css %}
body {
   background-color: lightgray;
}

div.post {
    margin: 30px;
    padding: 30px;
    box-shadow: 0 0 30px 10px #000000;
}
{% endhighlight %}

Den Bereich vor den geschweiften Klammern nennt man "Selektor", hier werden eine oder mehrere Regeln definiert. In den geschweiften Klammern definieren wir dann Schlüssel-Wert Paare (wie bei YAML) um bestimmte CSS Eigenschaften zu verändern. In diesem Fall verändern wir die folgenden Aspekte:

* Zunächst einmal die Hintergrundfarbe für die gesamte Seite. Der Selektor `body` ist das erste sichtbare HTML Element.
* Für den Bereich eines einzelnen Beitrages spielen wir zunächst ein bisschen an den Innen- und Außenabständen (engl. *padding* und *margin*) und setzen danach einen Schatteneffekt.

CSS Regeln werden normalerweise in spezielle Dateien geschrieben, die dann vom HTML Code aus geladen werden müssen. Zum Glück nutzen wir Jekyll und können auf das Templating-System zurückgreifen, so müssen wir den Ausschnitt von Zeile Nummer 6 nur einmal angeben.

{% highlight html linenos %}
<!DOCTYPE html>
<html>
  <head>
    <title>Test</title>
    <meta charset="utf-8"/>
    <link href="{{site.baseurl}}/css/style.css" type="text/css" rel="stylesheet" />
  </head>
  <body>
    <h1>{{ site.title }}</h1>
    {%raw%}{{ content }}{%endraw%}
  </body>
</html>
{% endhighlight %}

<div class="task">
    <div class="header">Aufgabe: Bindet einen CSS Stil in eure Seite ein</div>
    <ul>
      <li>Erzeugt einen Ordner <code>css</code> im obersten Ordner eurer Webseite. Legt in diesem Ordner eine Datei namens <code>style.css</code> an und kopiert die CSS Regeln von oben.</li>
        <li>Fügt die Referenz auf die neue CSS Datei in euer Standardlayout ein.</li>
    </ul>
</div>

### CSS Selektoren

CSS Selektoren wandern den Baum eures HTML Dokumentes ab um herauszufinden welche Regeln für welche Teile des Dokumentes berücksichtigt werden sollen. Die technischen Regeln dafür sind relativ simpel, sofern man sich die Baumstruktur eines HTML Dokumentes vor Augen führt. Wir werfen dazu mal einen genaueren Blick auf die Layout Datei, welche die einzelnen Beiträge anzeigt:

<div class="grid">
  <div class="col-1-2">
    {% highlight html %}
    {% raw %}
<div class="post">
  <header class="post-header">
    <h2 class="post-title">{{ page.title }}</h2>
    <p class="post-meta">
      {{ page.date | date: "%b %-d, %Y" }} •
      {{ page.author }}
    </p>
  </header>
  <article class="post-content">
    {{ content }}
  </article>
</div>
    {% endraw %}
    {% endhighlight %}
  </div>
  <div class="col-1-2">
    <div class="highlight">
      <pre>body
├── ...
└── div.post
    ├── header.post-header
    │   ├── h2.post-title
    │   └── p.post-meta
    └── article.post-content
      </pre>
    </div>
  </div>
</div>

|---------------|-----------------|-----------------------------------------------------------------------------|
|  Selektor     | Beispiel        |       Wirkung                                                               |
|---------------|-----------------|-----------------------------------------------------------------------------|
| E             | `body`          | Alle Elemente vom Typ E                                                     |
| E F           | `div h2`        | Alle Elemente vom Typ F unterhalb des Typs E                                |
| E > F         | `div > h2`      | Alle Elemente vom Typ F unmittelbar unterhalb des Typs E (im Beispiel leer!)|
| E:first-child | `p:first-child` | Das Erste Element unterhalb des Typs E                                      |
| E:hover       | `div:hover`     | Alle Elemente vom Typ E die unterhalb des Mauszeigers sind                  |
| E.*class*     | `div.post`      | Alle Elemente vom Typ E mit Klasse *class*                                  |
|---------------|-----------------|-----------------------------------------------------------------------------|

### Einige wichtige CSS Eigenschaften

[![Dokumentation im MDN]({{site.baseurl}}/assets/mdn.png)](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) `color`
: Farbe des Vordergrundes, typischerweise Schriftfarbe (HTML Farbcode)

[![Dokumentation im MDN]({{site.baseurl}}/assets/mdn.png)](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) `background-color`
: Farbe des Hintergrundes (HTML Farbcode)

[![Dokumentation im MDN]({{site.baseurl}}/assets/mdn.png)](https://developer.mozilla.org/en-US/docs/Web/CSS/text-align) `text-align`
: Ausrichtung des Textes (`left`, `right`, `center`)

[![Dokumentation im MDN]({{site.baseurl}}/assets/mdn.png)](https://developer.mozilla.org/en-US/docs/Web/CSS/text-decoration) `text-decoration`
: Striche unter, über oder durch den Text (`underline`, `overline`,  `line-through`, `underline overline`)

[![Dokumentation im MDN]({{site.baseurl}}/assets/mdn.png)](https://developer.mozilla.org/en-US/docs/Web/CSS/font-weight) `font-weight`
: Dicke des Texts (`normal`, `bold`, `lighter`, `bolder`)

[![Dokumentation im MDN]({{site.baseurl}}/assets/mdn.png)](https://developer.mozilla.org/en-US/docs/Web/CSS/border) `border`
: Breite, Struktur und Farbe der Umrandung (`1px solid red`)

[![Dokumentation im MDN]({{site.baseurl}}/assets/mdn.png)](https://developer.mozilla.org/en-US/docs/Web/CSS/box-shadow) `box-shadow`
: Schatten sind ein schon relativ kompliziertes Konstrukt. Wir definieren zunächst Horizontale und Vertikale Verschiebung um die "Richtung" des Schattens zu bestimmen. Dann folgen zwei Werte für Unschärfe und Breite des Schattens, zuletzt wird die Farbe definiert (`10px 10px 15px 15px red`).

`margin` und `padding`
: Innerer bzw. äußerer Abstand vom Rand, weiteres dazu im nächsten Kapitel


<div class="task">
  <div class="header">Aufgabe: Posts mit Stil</div>
  <ul>
    <li>Färbt den Hintergrund eurer gesamten Seite schwarz, die Schrift hingegen schwarz.</li>
    <li>Umrandet den gesamten Header eines Beitrages in einer beliebigen Farbe oder mit einem beliebigen Schatten. Wenn Ihr einen Schatten verwendet müsst Ihr ggfs. auch den äußeren Abstand (`margin-bottom`) anpassen.</li>
    <li>Hebt den Absatz, über dem sich der Zeiger des Besuchers befindet, irgendwie hervor.</li>
    <li>Hebt den ersten Absatz eines Beitrages hervor.</li>
    <li>Erzeug einen Selektor der alle Bilder mit der Klasse `preview` auf eine Größe von 64px * 64px festlegt. Verwendet statt der Eigenschaften `width` und `height` die Eigenschaften `max-width` und `max-height` und schaut was sich dadurch verändert.</li>
  </ul>
</div>
