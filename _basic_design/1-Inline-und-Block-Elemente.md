---
layout: post
author: Marcus
title: Inline und Block Elemente
description: >
  Bisher haben wir ein bisschen mit Farben gespielt, das eigentliche Layout der Seite aber nicht weiter angefasst. In diesem Kapitel beschäftigen wir uns mit dem Layout-Modell von CSS um unsere Seite vom Layout her ein wenig aufzupeppen.
css-files:
- /css/basic-design-0.css
css-rule-1: "background-color: red;"
css-rule-2: "background-color: lime;"
css-rule-3: "width: 200px; float: left;"
css-rule-4: "background-color: lime;"
---

{{ page.description }}

### Block vs. Inline

Bei Block-Elementen handelt es sich etwas vereinfacht gesagt um rechteckige Boxen. Für sie gelten im Normalfall unter anderem die folgenden Regeln:

* Wenn für sie keine explizite Breite gesetzt ist, versuchen Sie die gesamte Breite ihres Elterncontainers auszunutzen.
* Es lassen sich Innen und Außenabstände für die Box festlegen.
* Sofern keine Höhe gesetzt ist ergibt sich die Höhe aus der Höhe der Kindelemente.
* Wenn eine Box platziert wird, beginnt Sie auf einer "leeren Zeil". Oder andersrum gedacht: Nach einem Block Element wird ein Umbruch erzwungen.

<div>
  <div style="{{page.css-rule-1}}">Block Elemente nehmen die gesamte verfügbare Breite ein ...</div>
  <div style="{{page.css-rule-2}}">... und fügen sich "unter" den bisherigen Elementen ein.</div>
</div>

Beispiele für uns bekannte Block Elemente sind u.a: `div`, `p`, `ul`, `li`, `h1`. Das ist also der weitaus größere Teil der bisher kennengelernten Elemente. Trotzdem haben wir auch schon kurz mit Inline Elementen gearbeitet, nämlich mit `strong` und `em`. Für diese Elemente gelten unter anderem die folgenden Characteristika:

* Sie "fließen" quasi mit dem Text, haben also weder zwingend rechteckige Boxen noch erzeugen sie einen Zeilenumbruch.
* Ignorieren Regeln welche `margin-top` oder `margin-bottom` bzw. die `padding` Varianten für Abstand nach oben oder unten betreffen.
* Ignoreiren Regeln welche versuchen explizit Höhen oder Breiten festzulegen.

<p>
  <span style="{{page.css-rule-1}}">Ich fließe hier so entlang und <br /> breche auch mal viel zu früh um. Meine Box ist garantiert nicht rechteckig. </span><span style="{{page.css-rule-2}}">Der Textfluss nach mir kommt auch nicht auf eine neue Zeile sondern fließt so weit und breit wie er halt gerne möchte..</span>
</p>

### Float

Manchmal möchte man seine Seite aber trotzdem mit Elementen "nebeneinander" ausstatten. Das funktioniert indem wir die ersten beiden Regeln für Blockelemente ausstzen. Die Breite lässt sich explizit mit der Eigenschaft `width` festlegen, der Umbruch lässt sich über die Eigenschaft `float` steuern.

<div>
  <div style="{{ page.css-rule-3 }}{{ page.css-rule-1 }}">{{ page.css-rule-3 }}</div>
  <div style="{{ page.css-rule-2 }}">Ich bin ein <code>div</code> das weder <code>width</code> noch <code>float</code> Eigenschaften hat und verhalte mich daher wie gewohnt.</div>
</div>

Mit dieser Technik lassen sich also relativ einfach Navigationsbereiche umsetzen.

### Das CSS Box Modell

Um zu verstehen wie die einzelnen Abstände von Block Elementen berechnet werden muss man einen kurzen Blick auf das *CSS Box Model* werfen. Es zeigt anschaulich wie die Breiten von Padding, Border und Margin addiert werden um die endgültigen Dimensionen eines Block Elements zu bestimmen:

<img class="center" src="{{site.baseurl}}/assets/css-box-model.png" />

<div class="task">
  <div class="header">Aufgabe: Fließende Bilder</div>
  <ul>
    <li>Zeigt mindestens vier Bilder in einer Reihe an. Dazu benötigt Ihr die <code>float:left</code> Eigenschaft.</li>
    <li>Fügt unter jedes Bild eine kurze Beschreibung hinzu. Dazu benötigt Ihr ein <code>div</code> Element in dem das Bild und die passende Bildunterschrift zusammengefasst werden. Vermutlich müsst ihr ein wenig mit den Abständen des Box Model herumspielen, damit die Unterschriften auch visuell zu den Bildern passen.</li>
  </ul>
</div>
