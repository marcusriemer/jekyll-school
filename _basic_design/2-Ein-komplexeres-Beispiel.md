---
layout: post
author: Marcus
title: Komplexere CSS Regeln
description: >
  Das tolle an CSS: Man muss es nicht komplett verstehen, um davon zu profitieren ...
css-files:
- /css/basic-design-0.css
menu:
  css: >
    .menu {
      display: inline-block;
      box-shadow: 0 0 10px 1px rgba(0,0,0,0.6);
      border-radius: 6px;
      overflow: hidden;
      text-align: center;
      font-family:'noto serif',verdana,Arial;
    }

    .menu a {
      padding: 0 20px 0 30px;
      position: relative;
      text-decoration: none;
      line-height: 36px;
      background: -webkit-linear-gradient(#5DA6E1, #185282);
      background: -moz-linear-gradient(#5DA6E1, #185282);
      background: -o-linear-gradient(#5DA6E1, #185282);
      background: linear-gradient(#5DA6E1, #185282);
      color: white;
      display: block;
      float: left;
    }

    .menu a#active, .menu a:hover {
      background: -webkit-linear-gradient(#64D86A, #1D7521);
      background: -moz-linear-gradient(#64D86A, #1D7521);
      background: -o-linear-gradient(#64D86A, #1D7521);
      background: linear-gradient(#64D86A, #1D7521);
    }

    .menu a#active:after, .menu a:hover:after {
      background: linear-gradient(135deg,#64D86A, #1D7521);
    }

    .menu a:after {
      content:'';
      width: 36px;
      height: 36px;
      background: linear-gradient(135deg,#5DA6E1, #185282);
      position:absolute;
      top: 0px;
      z-index: 1;
      right: -18px;
      transform: scale(0.707) rotate(45deg);
      border-radius: 0 0 0 30px;
      box-shadow: 2px -2px 1px 1px rgba(0,0,0,0.5),
            3px -2px 1px 1px rgba(255,255,255,0.6);
    }

    .menu a:last-child:after {
      content:none;
    }

    .menu a:last-child {
      padding-right: 20px;
    }
---

{{ page.description }}

### Ein horizontales Menü
Das folgende (sehr kurze) Beispiel soll euch vor Augen führen, was mit purem CSS so alles an Magie möglich ist. Technisch gesehen kennt Ihr die meisten der verwendeten CSS Regeln, praktisch gesehen ist es dann meistens doch eher Aufgabe von speziell geschulten Designern solches CSS zu schreiben. Praktischerweise ist die Anwendung ansich aber wirklich trivial: CSS kopieren, ein `div` und ein paar Links einfügen, fertig! Dieses CSS Beispiel hab ich dem [Mozilla Demo Studio](https://developer.mozilla.org/en-US/demos/detail/breadcumb-menu-bar) entnommen.

<style>
  {{page.menu.css}}
</style>

<div class="grid">
  <div class="col-1-2">
    <h4>Der CSS Code</h4>
    {% highlight css %}
    {{page.menu.css}}
    {% endhighlight %}
  </div>
  <div class="col-1-2">
    <h4>Der HTML Code</h4>
    {% highlight html %}
<div class="menu">
  <a href="#menu1" id="active"> Menu 1</a>
  <a href="#menu1"> Menu 2</a>
  <a href="#menu1"> Menu 3</a>
  <a href="#menu1"> Menu 4</a>
</div>
    {% endhighlight %}
    <h4>Das Endergebnis</h4>
    <div class="menu">
      <a href="#menu1" id="active"> Menu 1</a>
      <a href="#menu1"> Menu 2</a>
      <a href="#menu1"> Menu 3</a>
      <a href="#menu1"> Menu 4</a>
    </div>
  </div>
</div>
