---
layout: post
author: Marcus
title: Seiten mit Bootstrap
layout: bootstrap
jumbotron: true
description: >
  Bootstrap ist ein zweischneidiges Schwert: Auf der einen Seite ist es eine wirklich umfassende Lösung komplizierte Webseiten aufzubauen, auf der anderen Seite sehen sich alle Seiten die Bootstrap verwenden fürchterlich ähnlich ... Der für diesen Kurs ausschlaggebende Grund ist aber ein ganz anderer: Bootstrap ist hervorragend dokumentiert und damit ein gutes Framework um so etwas zum ersten Mal auszuprobieren.
css-files:
- /css/bootstrap-examples.css
examples:
  grid1: >
    <div class="row">
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
      <div class="col-md-1">.col-md-1</div>
    </div>
    
    <div class="row">
      <div class="col-md-8">.col-md-8</div>
      <div class="col-md-4">.col-md-4</div>
    </div>
    
    <div class="row">
      <div class="col-md-4">.col-md-4</div>
      <div class="col-md-4">.col-md-4</div>
      <div class="col-md-4">.col-md-4</div>
    </div>
    
    <div class="row">
      <div class="col-md-6">.col-md-6</div>
      <div class="col-md-6">.col-md-6</div>
    </div>
---

### Herunterladen und Entpacken

Zum Zeitpunkt meiner Beschreibung auf dieser Seite ist Bootstrap Version 3.3, ladet euch also nach Möglichkeit diese Version runter. Auf der Seite von Bootstrap werden normalerweise <a href="http://getbootstrap.com/getting-started/#download" class="btn btn-primary btn-xs">drei verschiedene Downloads angeboten</a>, Ihr interessiert euch weder für die "SASS" noch für die "Source" Pakete. Sollten sich die Bezeichungen in der Zwischenzeit geändert haben sucht den Download über dem was von "compiled and minified" steht.

Das heruntergeladene Archiv sollte in etwa die folgenden Dateien enthalten:

    bootstrap-3.3.x-dist
    ├── css
    │   ├── bootstrap.css
    │   ├── bootstrap.css.map
    │   ├── bootstrap.min.css
    │   ├── bootstrap-theme.css
    │   ├── bootstrap-theme.css.map
    │   └── bootstrap-theme.min.css
    ├── fonts
    │   ├── glyphicons-halflings-regular.eot
    │   ├── glyphicons-halflings-regular.svg
    │   ├── glyphicons-halflings-regular.ttf
    │   ├── glyphicons-halflings-regular.woff
    │   └── glyphicons-halflings-regular.woff2
    └── js
        ├── bootstrap.js
        ├── bootstrap.min.js
        └── npm.js

Kopiert bitte die `bootstrap.min.css` und die `bootstrap-theme.min.css` Datei in den CSS Ordner eurer Seite. Die Datei `bootstrap.min.js` kommt in einen Ordner `js` eurer Seite (diesen müsst ihr ggfs. noch anlegen).

### Einbinden in eure Templates

Wir machen es uns zunächst mal leicht und verwenden Bootstrap nur in einem sehr kleinen Teil der Seite um mal ein Gefühl dafür zu entwickeln. Legt daher im Ordner `_layouts` eine neue Datei namens `bootstrap.html` an und fügt den folgenden Inhalt hinein:

{% highlight html %}
  {% include_relative snippets/theme-layout.txt %}
{% endhighlight %}

### Und was mache ich jetzt mit Bootstrap?

Das ist die Stelle, an der ich auf die <a href="http://getbootstrap.com/css/" class="btn btn-primary btn-xs">hervorragende Dokumentation verweisen kann</a> ;) Bootstrap liefert wirklich nahezu alles mit, was man sich so wünschen kann. Es gibt übrigens auch eine <a href="http://holdirbootstrap.de/" class="btn btn-primary btn-xs">deutsche Version der Dokumentation</a>, Ich habe aber keine Ahnung wie gut die Übersetzung ist. Detailliert eingehen möchte ich an dieser Stelle aber besonders auf das Grid System und den Zusammenhang mit der Entwicklung für mobile Geräte.

### Grids und Mobile first

Typischerweise teilen sich Webseiten in mindestens zwei verschiedene Bereiche auf: Ein Bereich stellt den Inhalt dar, der andere Bereich die Navigation. Wenn man seine Seite vertikal gestaltet ist es ziemlich einfach diese Bereiche zu trennen: Man braucht einen `div` Bereich für die Navigation "oben" und einen `div` Bereich für die Navigation "darunter". Problematisch wird es aber garantiert, wenn die Anzahl der Seiten zwischen denen man navigieren möchte einfach zu groß wird. Deswegen sieht man auf so vielen Seiten eine sog. "Navbar". Nach unten hin ist der Platz im Browser durch die Scrollbalken theoretisch beliebig.

<div class="row">
  <div class="col-md-7">
    <div class="thumbnail">
      <div class="caption">
        <h4>Seite im Desktop Browser</h4>
      </div>
      <img src="{{site.baseurl}}/assets/vertical-nav-desktop.png">
    </div>
  </div>
  <div class="col-md-5">
    <div class="thumbnail">
      <div class="caption">
        <h4>Seite im mobilen Browser</h4>
      </div>
      <img src="{{site.baseurl}}/assets/vertical-nav-mobile.png">
    </div>
  </div>
</div>
Wer sich schon mit der [zweiten CSS Lektion]({{site.baseurl}}/Gestaltung/1-Inline-und-Block-Elemente/) befasst hat kennt eine weitere Möglichkeit eine Seitennavigation zu bauen. Dort haben wir die Eigenschaften `float` und `margin` verwendet, was auf kleinen Bildschirmen wie den von Mobilgeräten aber unpraktisch ist. Deswegen verwenden wir an dieser Stelle das Grid-System von Bootstrap.

{% highlight html %}
  {{ page.examples.grid1 }}
{% endhighlight %}

<div class="show-grid">
  {{ page.examples.grid1 }}
</div>

<div class="well well-sm">
  <h4>Aufgabe: Ein Bootstrap Layout mit Sidebar</h4>
  <p>Mit diesem System lassen sich komplex verschachtelte Webseiten bauen, für den Anfang reicht uns aber erstmal eine Zweiteilung.</p>
  <ul>
    <li>Sofern schon nicht geschehen: Ladet euch Bootstrap runter und legt ein Layout an wie im Beispiel gezeigt.</li>
    <li>Baut in das von euch erstellte Layout einen Bereich für die Sidebar ein (ob links oder rechts ist euch überlassen). Der entsprechend "andere" Bereich muss natürlich auch erstellt werden, hier gehört der {% raw %}<code>{{ content }}</code>{%endraw%} Bereich hin.</li>
    <li>Werft einen Blick auf die <a href="http://getbootstrap.com/components/#nav" class="btn btn-primary btn-xs">Navs</a> von Bootstrap. Hier findet Ihr alles was Ihr für die Navigation braucht. Technisch gesehen handelt es sich um einfache Listen die halt per CSS ein bisschen im Aussehen verändert werden.</li>
    <li>Sofern Ihr euch schon mit den Aufgaben zur Programmierung befasst habt, könntet Ihr die Navigation sogar passend zur entsprechenden Unterseite hervorheben. Dazu könnt Ihr für den Anfang mit einem einfachen Vergleich arbeiten innerhalb des <code>class</code> Attributes arbeiten: </code></li>
  </ul>
</div>


<div class="well well-sm">
  <h4>Für Programmierer: Hervorhebungen in der Navigation</h4>
  <ul>
    <li>In Bootstrap lässt sich das aktuell aktivierte Navigationselement mit <code>class="active"</code> hervorheben. Da der Wert <code>active</code> aber nicht immer geschrieben werden soll, könnt Ihr die aktuelle URL mit der Ziel URL vergleichen. Für den Anfang reicht ein statischer Ausdruck der für jeden Link in etwa wie folgt aussieht: <code>{% raw %}{% if page.url == "/meine-seite" %}active{% endif %}{% endraw %}</code>. Den Verleich gegen <code>"/meine-seite"</code> müsst Ihr natürlich für jeden Link anpassen.</li>
    <li>Falls Ihr euch schon mit Schleifen befasst haben solltet geht die Hervorhebung noch eleganter. Legt in der <code>_config.yml</code> eine Liste für alle Seiten an, zu denen navigiert werden können soll. Diese Liste gebt Ihr dann innerhalb der Navigation mit den passenden <code>li</code> Elementen aus. Beachtet dabei, das eure Listenelemente mindestens zwei Informationen bereitstellen sollten: Die URL des Links und eine für Menschen sinnvolle Bezeichnung.</li>
    <li>Bei dem Vergleich im <code>if</code> könnt Ihr dann die statisch codierten URLs rausschmeißen und gegen die URL der aktuellen Laufvariable ersetzen. Sollte eure Laufvariable <code>currentNav</code> heißen und die entsprechende URL einfach <code>url</code>, erhaltet ihr also den folgenden Ausdruck: <code>{% raw %}{% if page.url == currentNav.url %}active{% endif %}{% endraw %}</code></li>
  </ul>
</div>

<div class="well well-sm">
  <h4>Für Neugierige: Integriert ein Bootstrap Beispiel</h4>
  <p>Auf der Seite mit den <a href="http://getbootstrap.com/examples/blog" class="btn btn-primary btn-xs">Bootstrap Beispielen</a> finden sich diverse  gute Grundlage für Seiten mit verschiedenen Arten von Navigationskonzepten. Sucht euch davon eines aus, das euch spannend erscheint.</p>
  <ul>
    <li>Schaut euch an, wie sich das gewählte Beispiel verhält, wenn es mit einem mobilen Browser besucht wird. Lasst dafür aber gerne das Handy stecken und verwendet die entsprechenden Tools im Firefox.</li>
  </ul>
</div>
