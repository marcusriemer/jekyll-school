---
layout: post
author: Marcus
title: Entwicklertools im Firefox
description: >
  Die meisten Webbrowser kommen mit einer Reihe sehr mächtiger Werkzeuge, die bei der Webentwicklung hilfreich sind. Am Beispiel der Entwicklertools im Firefox wird in diesem Beitrag kurz erläutert, wie Ihr euch das Leben leichter machen könnt.
---
{{page.description}}

<img class="responsive" src="{{ site.baseurl }}/assets/tools-firefox.png">

Auf dem obigen Screenshot seht Ihr schon einige Funktionen der Firefox Entwicklerwerkzeuge. Diese können wahlweise über das Firefox Menü oder die Tastenkombination <kbd>Strg</kbd> + <kbd>Shift</kbd> + <kbd>I</kbd>.
 
Auf dem Screenshot wird die Seite im "Responsive Mode" dargestellt. Dieser Modus hilft euch dabei abzuschätzen, wie eure Seiten z.B. auf Mobilgeräten oder allgemein auf anderen Bildschirmen aussehen würden. Um den Modus zu aktivieren klickt Ihr auf den vierten Knopf von rechts in der Menüleiste der Entwickertools oder drückt <kbd>Strg</kbd> + <kbd>Shift</kbd> + <kbd>M</kbd>.

Der momentan sichtbare Bereich der Entwicklertoos ist der sog. "Inspektor". Hier seht ihr die HTML Struktur eurer Seite im Details, was besonders praktisch ist wenn man viele verschiedene Templates benutzt. Der Bereich rechts vom Inspektor hilft beim durchschauen der momentan angewendeten CSS Regeln.