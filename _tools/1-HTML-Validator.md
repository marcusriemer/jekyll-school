---
layout: post
author: Marcus
title: Seiten auf Korrektheit prüfen
description: >
  Browser versuchen im Normalfall eine Webseite unter allen Umständen irgendwie darzustellen, sind sich dabei aber nicht immer einig wie verschiedene Fehler bereinigt werden sollten. Daher ist es wichtig, die eigene Seite regelmäßig auf formelle Fehler zu prüfen.
---

{{ page.description }}

### Validatoren vom W3C

Es gibt vom W3C (World Wide Web Consortium), das sind die Leute die entscheiden wie korrekte Webseiten technisch auszusehen haben, eine Reihe von verschiedenen Webseiten mit denen man seine eigene Seite testen kann. Dummerweise haben diese Seiten keine Möglichkeit eure lokal betrieben Seiten direkt zu prüfen, Ihr müsst die Dateien von Hand hochladen.

* Um die HTML Syntax einer Webseite zu kontrollieren verwendet Ihr am Besten den [Markup Validation Service](http://validator.w3.org/#validate_by_upload).
* CSS Regeln kann man entsprechend mit dem [CSS Validation Service](http://jigsaw.w3.org/css-validator/#validate_by_upload) überprüfen.

Die von Jekyll erstellten HTML Dateien befinden sich standardmäßig im Ordner `_site`, schaut am Besten auf die URL im Browser um den richtigen Ordner mit der richtigen Datei zu finden. Alternativ könnt Ihr euch im Firefox die [Web Developer](https://addons.mozilla.org/en-US/firefox/addon/web-developer/?src=collection&collection_id=da0ecd99-2289-7ab0-7d57-e7c489c845c3) Erweiterung installieren. Dort gibt es im Menü `Tools` die Option "Display Page Validation" um immer auf HTML bzw. CSS Fehler aufmerksam gemacht zu werden.

<div class="stacked">
  <img src="{{site.baseurl}}/assets/html-validator-correct.png" />
  <img src="{{site.baseurl}}/assets/html-validator-errors.png" />
</div>