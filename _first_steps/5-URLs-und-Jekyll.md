---
layout: post
author: Marcus
title: URLs und Jekyll
description: >
  Langsam wird es Zeit mehr als nur eine einzige Seite darstellen zu können. Dieser Beitrag geht kurz darauf ein, wie URLs in Jekyll verwaltet werden.
---
<style>
  /* Horrible hack to style Markdown table ... */
  table td:nth-child(1) {
    width: 150px;
  }
  table td:nth-child(2) {
    width: 250px;
  }
</style>


{{ page.description }}

### URLs und Dateisysteme

Pfadangaben wie `C:\Windows` kennt Ihr vermutlich alle von eurem eigenen Rechner, URLs wie `http://www.google.de` aus dem Alltag. Wenn man selber Webseiten entwickelt ist es wichtig sich zumindest oberflächlich klar zu machen was eine URL ist und wie sie mit dem Dateisystem zusammenhängt.

    foo://sub.domain.com:8042/over/there/index.dtb?type=animal&name=narwhal#nose
    \_/   \____________/ \__/\__________/\_______/\_______________________/ \__/
     |        Host        |    Ordner      Datei      Query Parameter        |
    Scheme               Port                       (Schlüssel-Wert Paare)   |
                             \___________________/                       Fragment
                                      Pfad

| Bereich | Beispiele für typische Werte | Bedeutung                                                 
|---------|------------------------------|-----------------------------------------------------------
| Scheme  | `https`, `http`, `ftp`       | Zugriffsart, `http` für Dokumente, `ftp` für Dateien
| Host    | `www.name.de`                | Der Name unter dem ein Server im Internet erreichbar ist.
| Port    | `80 (http), 443 (https)`     | Auf einem Server können mehrere Dienste laufen, die durch die Portnummer unterschieden werden. Die für eine Zugriffsart typische Portnummer ist optional.
| Pfad    | `/post/2015-02-09/index.html`| Eine Pfadangabe bestehend aus Ordner und Dateien. Wird der Dateiname weggelassen kommt normalerweise implizit der Wert `index.html` zum Einsatz.
| Query Parameter | beliebig             | Dynamische Webseiten müssen vom Benutzer Eingaben entgegennehmen können, oft geschieht das über spezielle Schlüssel-Wert Paare in der URL.
| Fragment| `#content`                   | Stellt sicher, dass das HTML Element mit dem entsprechenden `id` Attribut sichtbar ist.

Weil wir uns in diesem Kurs nur mit statischen Seiten beschäftigen, haben die Query Parameter für uns erstmal keine Bedeutung. Wir beschäftigen uns jetzt aber genauer mit der Pfadangabe.

### Permalinks

Üblicherweise ist man daran interessiert möglichst "schöne" URLs für die eigene Seite zu verwenden. Im besten Falle vermittelt Sie einem Besucher schon im Vorraus, was Ihn hinter dem Link erwarten wird. Technische Details wie Dateiendungen werden dabei als störend empfunden, sind für die Arbeit an der Webseite aber trotzdem wichtig. Jekyll umgeht dieses Dilemma, indem es für jede Datei die eine sprechende URL haben soll einen eigenen Ordner anlegen kann.

Die URLs die von Jekyll erzeugt werden lassen sich über die `config.yml` steuern. Standardmäßig eingestellt ist die Vorlage `date`, welche URLs nach dem Schema `/:categories/:year/:month/:day/:title.html` generiert. Um dieses Verhalten zu verändern, kann man den Schlüssel `permalink` mit den [dokumentierten Bausteinen verändern](http://jekyllrb.com/docs/permalinks/#template-variables).

### Das Dateisystem

Grundsätzlich befasst sich Jekyll nur mit Dateien die über ein Frontmatter verfügen oder deren Dateipfad mit einem Unterstrich beginnt. Dadurch könnt Ihr beliebig eigene Ordnerstrukturen anlegen und Dateien daraus laden, es bieten sich aber die folgenden Konventionen an:

* Ein Ordner `css` für alle möglichen Dateien die zum Design der Seite beitragen.
* Ein Ordner `js` für alle Javascript Dateien die für eure Seite eine Rolle spielen.
* Ein Ordner `assets` für Bilder, Videos, Musikdateien, ... die in Beiträgen verwendet werden.
* Hauptseiten (wie die vorhandene `index.html`) kommen in den obersten Ordner und haben einen sprechenden Dateinamen (`about_me.html`).

Wer nicht vom Dateisystem auf die URL schließen möchte hat auch eine andere Möglichkeit. Wenn man im YAML Frontmatter den Schlüssel `permalink` vorgibt, kann man den Pfadteil der URL vom Dateisystem entkoppeln. Jekyll sorgt dann dafür, dass die Datei unter dem von euch vorgeschriebenen Pfad erreichbar ist.

### Links zu eigenen Inhalten

Natürlich wisst Ihr nicht immer im Voraus mit welcher Zugriffsart, unter welcher Domain und an welchem Port eure Seite später mal erreichbar sein könnte. Deswegen gibt es die Möglichkeit nur hintere Teile einer URL anzugeben, die vorderen Teile werden dann vom Browser von der aktuellen URL übernommen. Typischerweise nutzt man z.B. eine URL wie `<a href="/">...</a>` um auf die oberste Ebene (oder konkret die "oberste" `index.html`) eurer Seite zu verlinken.

Ihr könnt allerdings nicht voraussetzen, dass Ihr immer die volle Kontrolle über den Pfad habt unter dem eure Webseite später erreichbar ist. Deswegen empfiehlt es sich in der `config.yml` einen Schlüssel mit dem Wert `baseurl` anzulegen und diesen Wert vor alle eure Links zu schreiben. Konkret solltet Ihr euch also angewöhnen `<a href="{{site.baseurl}}/">...</a>` zu schreiben. Wenn ihr eure Datei zum Beispiel später unter `http://stud.fh-wedel.de/~schueler1/` hosten wollt, müsst Ihr den Wert `baseurl` in der `config.yml` also auf `/~schueler1` einstellen.

<div class="task">
  <div class="header">Aufgabe: Eine Seite über Euch</div>
  <ul>
    <li>Ihr habt euch nun schon einige Male im Rahmen von einzelnen Beiträgen beschrieben. Legt nun stattdessen "neben" der <code>index.html</code> eine Datei mit eurem Autorennamen an und beschreibt euch dort selber.</li>
    <li>Verlinkt diese Seite von der Hauptseite mit einer Beschreibung wie "Über mich" oder ähnliches.</li>
    <li>Ändert das Schema für die erzeugten Beiträge so ab, dass es nicht mehr auf <code>.html</code> endet und den Tag an dem der Beitrag veröffentlicht wurde ignoriert.</li>
  </ul>
</div>

