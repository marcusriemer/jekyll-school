---
layout: post
author: Marcus
title: Templating und Jekyll
description: >
  Schluss mit dem Bearbeiten von einfachen Beiträgen, jetzt wollen wir uns um die Seite als ganzes kümmern! Wir werfen einen Blick auf das Templating-System von Jekyll und basteln (endlich) einen funktionierenden "Zurück" Link in eure Seite.
---
{{ page.description }}

### Der _layouts Ordner und die {%raw%}{{content}}{%endraw%} Anweisung

Bisher haben wir uns bei der Erkundung von Jekyll auf den ` _posts` Ordner und die `_config.yml` Datei beschränkt. Trotzdem war um den eigenen Beitrag "herum" ja immer noch ein bisschen mehr Inhalt, z.B. der Name des Autors und der Name der Webseite als Ganzes. Die meisten Webseiten verwenden auf die ein oder andere Art ein System aus verschachtelten Vorlagen (engl. *Templates*) und auch unsere Seite ist keine Ausnahme.

Im `_layouts` Ordner finden sich zwei Dateien: `default.html` und `post.html`. In diesen Dateien stehen noch ein paar HTML Elemente, auf die wir bisher nicht vertieft eingegangen sind:

* An der Datei für das standardmäßige (engl. *default*) Layout sieht man die vollständige Struktur einer HTML Datei. Wir haben uns bisher nur mit HTML das zwischen `<body>...</body>` steht befasst, dieser Bereich wird direkt im Browserfenster dargestellt. Das Element `head` steht für einen Bereich mit zusätzlichen Informationen, die nicht direkt angezeigt werden..
    * Den Text im `title` Element seht Ihr aber auf den Tabs bzw. an der Fensterleiste des Browsers.
    * Durch `meta` Informationen können dem Browser weitere Hinweise zur Verarbeitung gegeben werden, in diesem Fall sorgt die Schlüssel-Wert Kombination `charset="utf-8"` dafür, dass wir auf unseren Seiten Umlaute (ä,ö,ü) verwenden können.
* An der Datei für die Beiträge (engl. *post*) sieht man häufig das Attribut `class`. Mit diesem Attribut werden wir uns im Rahmen des Webdesigns befassen.

<div class="grid">
  <div class="col-1-2">
    <h4>_layouts/default.html</h4>
    {% highlight html %}
    {% include_relative snippets/template_default.txt %}
    {% endhighlight %}
  </div>
  <div class="col-1-2">
    <h4>_layouts/post.html</h4>
    {% highlight html %}  
    {% include_relative snippets/template_post.txt %}
    {% endhighlight %}
  </div>
</div>

Das Besondere an diesen Template Dateien ist die Verwendung des Ausdrucks `{%raw%}{{ content }}{%endraw%}`. An dieser Stelle wird der Inhalt eines Dokumentes, sofern im Frontmatter das entsprechende `layout` definiert ist, einfach eingesetzt. Wenn wir also einen Beitrag schreiben, wird dieser in zwei verschiedene Dateien eingesetzt:

* Zunächst in die Datei `_layouts/post.html`, dadurch sehen alle "normalen" Blogbeiträge gleich aus.
* Weil die Datei `_layouts/post.html` aber seinerseits auf das layout `default` verweist, kommt auch die Datei `_layouts/default.html` ins Spiel.

<div class="task">
  <div class="header">Aufgabe: Ein Link zur Startseite</div>
  <ul>
    <li>Bearbeitet eine der beiden Vorlagendateien so, dass von jeder Unterseite immer mit nur einem Klick zurück zur Startseite navigiert werden kann. Verwendet dazu die folgende Vorlage: {% raw %} <code>href="{{site.baseurl}}/"</code>{% endraw %}. Im nächsten Kapitel wird auf die Bedeutung hiner dieses Konstrukts genauer eingegangen.</li>
    <li>Ergänzt die Vorlagedatei für Beiträge so, dass unter jedem Beitrag in einem eigenen Absatz eine Werbenachricht wie "Diese Seite wird Ihnen präsentiert von: Hundefutter" oder ähnliches steht.</li>
    <li>Für Fortgeschrittene: Verwendet in der Werbenachricht wahlweise eine Variable aus der Kategorie <code>page</code> oder <code>site</code>.
  </ul>
</div>
