---
title: Markdown zu HTML
layout: post
author: Marcus
description : >
  Nachdem wir verstanden haben, wie man Jekyll in Betrieb nimmt und damit neue Beiträge veröffentlicht werfen wir nun einen Blick auf HTML. Dabei werden wir zunächst unseren Markdown Quelltext nach HTML übersetzen und uns dann mit neuen HTML Konstrukten befassen.
---

{{ page.description }}

### HTML Elemente

HTML Seiten haben immer eine sogenannte "Baumstruktur", die im Quelltext aussieht wie folgt:

{% highlight html %}<div>
    <p>Ich bin ein Absatz mit mal <em>kursivem</em> und mal <strong>fettem</strong> Text</p>
    <ul>
        <li>Ich bin ein <em>kursiv betontes</em>Listenelement.</li>
        <li>Ich bin ein <strong>fett betontes</strong> Listenelement.</li>
    </ul>
</div>
{% endhighlight %}

Dabei spielen die Leerzeichen in der Quelltexstruktur im Regelfall keine Rolle, das einzig relevante ist die "Verschachtelung" der sog. HTML Elemente. Ein Element beginnt dabei immer mit einem in spitzen Klammern geschriebenen `<Elementnamen>`. Ein Element beschreibt einen Bereich, der sich bis zu dem passenden schließenden `</Elementnamen>` (man beachte den Schrägstrich) erstreckt.

<dl>
    <dt>Container mit <code>div</code></dt>
    <dd>
        Das <code>div</code> Element ist eine Möglichkeit inhaltlich zusammenhängende Elemente grundlegend zu Strukturieren. Es hat standardmäßig keinerlei eigenes Aussehen, kann aber grafisch verändert werden. Von diesem Umstand werden wir ausführlich Gebrauch machen, sobald es um die grafische Gestaltung unserer Seite geht.
    </dd>
    <dt>Absätze mit <code>p</code></dt>
    <dd>
        Das <code>p</code> Element bezeichnet einen Absatz. Wie in Word ist damit ein zusammenhängender Textblock gemeint, der im Regelfall am Ende ein wenig Freiraum bis zum nächsten Absatz lässt.
    </dd>
    <dt>Formatierung mit <code>em</code> und <code>strong</code></dt>
    <dd>
        <code>em</code> steht für Emphasis (dt. <em>Betonung</em>) und stellt den Text üblicherweise kursiv dar, <code>strong</code> ist eine andere Art von Betonung und wird üblicherweise fett gedruckt dargestellt.
    </dd>
    <dt>Listen mit <code>ul</code> und <code>li</code></dt>
    <dd>
        Das <code>ul</code> steht für <i>unordered list</i> (im Gegensatz zu <code>ol</code> für <i>ordered list</i>) und bezeichnet einen Bereich in dem Dinge aufgelistet werden sollen. Jeder einzelne Aufzählungspunkt muss dabei von einem <code>li</code> Element (für <i>list item</i>) umschlossen werden.
    </dd>
</dl>

<div class="task">
    <div class="header">Aufgabe: Übersetzt euren vorigen Markdown-Eintrag nach HTML</div>
    <ul>
        <li>Legt wieder einen neuen Beitrag im <code>_posts</code> Ordner an, verwendet dabei ein beliebiges Datum und einen beliebigen Titel, aber auf jeden Fall die Dateiendung <code>.html</code> statt <code>.md</code></li>
        <li>Übersetzt den vorigen Markdown Beitrag über eure Person durch das entsprechende HTML Äquivalent, den Link lasst Ihr dabei erstmal weg. Im Optimalfall sollten beide Beiträge bis auf den Titel, das Datum und den fehlenden Link exakt identisch aussehen.</li>
    </ul>
</div>


### HTML Attribute
Teilweise müssen HTML Elemente mit exakteren Informationen versorgt werden. In diesen Fällen kommen die sog. Attribute zum Einsatz. Dabei handelt es sich wie bei YAML um einen Schlüsel und einen Wert, die jedoch leicht anders aufgeschrieben werden. Als Beispiel nehmen wir uns einmal das HTML Element <code>a</code> mit dem man auf andere Seiten verlinken kann.

    <a href="http://www.marcusriemer.de/">Marcus Homepage</a>

Wir sehen, dass beim Attribut `href` der Wert in Anführungszeichen gesetzt wird und zwischen Wert und Schlüssel ein Gleichheitszeichen steht.

Die Einbindung von Bildern geht mit dem `<img src="URL"/>` Attribut, welches eine weitere Besonderheit verdeutlicht: Nicht alle HTML Elemente haben einen Inhalt. Bei solchen Elementen wird schon beim öffnenden Element hinten ein Schrägstrich geschrieben. Um also ein Bild von der Wikipedia zu eurem Hobby einzubinden müsst Ihr nur das `src` Attribut mit dem richtigen Pfad füttern. Für das Bild [des Roguelikes "Vulture"](https://en.wikipedia.org/wiki/Roguelike#mediaviewer/File:Vultures-2.1.0_screenshot.jpg) lautet eine URL z.B. <code>https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Vultures-2.1.0_screenshot.jpg/640px-Vultures-2.1.0_screenshot.jpg</code>.

Sollte das von euch eingebundene Bild zu groß oder zu klein sein, könnt Ihr die Größe der Darstellung mit den Attributen `width` und `height` beinflussen. Als Werte kommen dabei entweder Pixel zum Einsatz (z.B. `"200px"`) oder relative Prozentangaben (z.B. `"50%"`).

<div class="warn">
  Verwendet keine relativen Prozentangaben für das <code>height</code> Attribut, das wird (fast) nie den gewünschten Effekt haben.
</div>

<div class="task">
    <div class="header">Aufgabe: Verlinkt ein Bild</div>
    <ul>
        <li>Fügt in euren Beitrag ein beliebiges Bild von der Wikipedia ein. Dazu müsst Ihr ggfs. den sog. "Media Viewer" der Wikipedia mit einem Klick auf "mehr Details" in den Hintergrund verbannen. Unter dem eigentlichen Bild sind dann verschiedene Links für die unterschiedlichen Größen des Bildes.</li>
        <li>Verlinkt dieses Bild indem Ihr ein <code>&lt;a href="..."&gt;...&lt;/a&gt;</code> Element um das Bild herumlegt.</li>
    </ul>
</div>
