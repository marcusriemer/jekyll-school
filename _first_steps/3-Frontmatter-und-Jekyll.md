---
layout: post
author: Marcus
title: Frontmatter und Jekyll
description: >
  Bisher haben wir uns nur oberflächlich mit vordefinierten Werten im Frontmatter beschäftigt. Jetzt wollen wir uns einmal anschauen, was man mit diesen Werten alles bewirken kann.
---
{{ page.description }}

### Zugriff auf einfache Werte

Um auf einen Wert aus dem Frontmatter zuzugreifen, muss der entsprechende nur in geschweiften Klammern mit dem Präfix `page` in den Quelltext geschrieben werden. Um also den Namen des Autors im Text anzuzeigen schreiben wir an der entsprechenden Stelle im Dokument einfach `{% raw %}{{ page.author }}{% endraw %}`. Das funktionert mit allen einfachen Werten wie z.B. Wörtern, Sätzen oder Zahlen. Für nicht ganz so einfache Werte wie Aufzählungen werden wir uns später noch andere Möglichkeiten anschauen müssen.

### Zugriff auf verschachtelte Werte

Manchmal möchte man Werte die inhaltlich zusammengehören logisch gruppieren. Um das zu tun, werden die zusammengehörigen Werte mit zwei Leerzeichen eingerückt. Der Zugriff erfolgt dann über den Namen der übergeordneten Kategorie. Die YAML Daten für meine Person könnten z.B. aussehen wie folgt:

{% highlight yaml %}
vorname: Marcus
nachname: Riemer
alter: 26
geboren:
  tag: 5
  monat: September
  jahr: 1988
{% endhighlight %}

Der Zugriff auf den Tag meiner Geburt würde dann aussehen wie folgt: `{% raw %}{{page.geboren.tag}}{% endraw %}`. Bei genauerer Betrachtung stellen wir also fest, dass auch dieses ominöse Präfix `page` nichts weiter ist als eine Kategorie. Es handelt sich dabei um die Kategorie aller YAML Informationen auf der aktuellen Seite.

### Ein Blick auf die globale Konfiguration

Manche Informationen benötigt man in mehr als nur einem Beitrag, zum Beispiel eure Benutzernamen auf Twitter, einen kurzen Text der euch als Autor beschreibt, ... Immer wenn das der Fall ist, können die entsprechenden Werte in der Datei `_config.yml` für alle Bereiche definiert werden. Wie die Endung `.yml` schon vorwegnimmt, handelt es sich dabei um eine Textdatei in der mit YAML beschriebene Werte stehen. Auf die in dieser Datei beschriebenen Werte könnt ihr über die Kategorie `site` zugreifen. Um also in einem Beitrag den Titel eurer Seite auszugeben, verwendet Ihr `{% raw %}{{site.title}}{% endraw %}`.


<div class="warn">
Die in der <code>config.yml</code> hinterlegten globalen Werte werden nur einmal mit dem Start des Servers geladen. Wenn ihr also Änderungen an diesen Werten vornehmt, müsst Ihr den Server danach neu starten.
</div>

<div class="task">
    <div class="header">Aufgabe: Stellt euch in einem Beitrag vor</div>
    <ul>
        <li>Legt wieder einen neuen Beitrag im <code>_posts</code> Ordner an, verwendet dabei ein beliebiges Datum und einen beliebigen Titel. Ob ihr die Datei mit der HTML Schreibweise (Endung <code>.html</code>) oder der Markdown Schreibweise (Endung <code>.md</code>) versehen wollt ist euch freigestellt.</li>
        <li>Hinterlegt im Frontmatter beliebig strukturierte YAML Daten zu eurer Person und gebt diese aus.</li>
        <li>Gebt außerdem den Namen der Seite aus, wie er in der Datei <code>_config.yml</code> hinterlegt ist. Das Präfix um auf diese YAML Daten zuzugreifen lautet <code>site</code></li>
    </ul>
</div>
