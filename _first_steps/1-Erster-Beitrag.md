---
title: Der erste neue Beitrag
layout: post
author: Marcus
description: >
  Nachdem Jekyll nun läuft, können wir uns daran machen auf der entstandenen Seite den ersten Beitrag zu veröffentlichen.

paths:
  site: D:\web\site
  jekyll: D:\web\jekyll

---

Beginnt mit dem Download [des Beispielarchivs](http://git.gurxite.de/jekyll-school/get/pupil-start.zip). Solltet Ihr in der FH arbeiten entpackt Ihr es bitte unter `{{ page.paths.site }}`. Das Archiv enthält die folgende Dateistruktur:

    site
    ├── _config.yml
	├── develop.cmd
    ├── index.html
    ├── _layouts
    │   ├── default.html
    │   └── post.html
    └── _posts
        └── 2014-09-06-Hallo-Welt.md

Wir wollen zunächst einmal prüfen, was die hier gezeigte Webseite eigentlich beinhaltet. Dazu müssen wir Jekyll im Servermodus starten.

<div class="warn">
  Zuhause müsst Ihr die Dateien natürlich nicht alle nach <code>D:\web</code> packen. Es ist aber wichtig, dass der <code>jekyll</code> Ordner exakt so heisst und das der Ordner der jeweiligen Seite (hier: <code>site</code>) im gleichen Ordner wie der <code>jekyll</code> Ordner ist.
</div>

<div class="task">
    <div class="header">Aufgabe: Der erste Start von Jekyll</div>
    <ul>
        <li>Ladet das Beispielarchiv herunter und entpackt es nach <code>{{ page.paths.site }}</code>.</li>
        <li>Öffnet die Kommandozeile im eben verwendeten Ordner (Doppelklick auf die <code>develop.cmd</code> im Ordner <code>{{page.paths.site}}</code>)</li>
        <li>Startet Jekyll mit der Eingabe <code>jekyll serve --watch</code></li>
        <li>Öffnet im Browser die Seite <a href="http://localhost:4000"><code>http://localhost:4000</code></a></li>
        <li>Schaut euch den ersten (und einzigen) Beitrag auf der Seite im Browser an.</li>
    </ul>
</div>

Am Ende dieses Kurses werdet Ihr die Funktion (und den Inhalt) jeder Datei im Ordner `{{ page.paths.site }}` verstehen. Für den Anfang werden wir uns aber darauf beschränken eurer Seite einen weiteren Beitrag hinzuzufügen. Öffnet dazu in einem Editor eurer Wahl die Datei `_posts/2014-09-06-Hallo-Welt.md`.

Ganz oben in der Datei, gewissermaßen "eingequetscht" zwischen den `---` Zeichen befindet sich die sogenannte "Frontmatter" Sektion. Dieser Bereich enthält strukturierte Metadaten über den aktuellen Beitrag, der an anderer Stelle von Jekyll verwendet werden kann. Das Schema dieser YAML Eintrage lautet dabei immer `Schlüssel:Wert`. Wir nennen diese Daten strukturiert, weil wir über den Namen des Schlüssels eindeutig auf sie zugreifen können. Welche Werte als Schlüssel oder Wert verwendet werden ist prinzipiell eine Frage der Anwendung.

<div class="task">
    <div class="header">Aufgabe: Kleinere Änderungen am Frontmatter</div>
    <p>Schaut was passiert, wenn Ihr die folgenden Änderungen vornehmt:</p>
    <ul>
        <li>Ändert den Wert des Schlüssels <code>author</code> auf euren Namen.</li>
        <li>Fügt einen neuen Wert "Erste Seite" mit dem Schlüssel <code>title</code> zum Frontmatter hinzu.</li>
        <li>Ändert den Wert des Schlüssels <code>layout</code> auf "default"</li>
    </ul>
</div>

Die weitere Datei ist in einem sogenannten "Markdown Dialekt" geschrieben, den ihr möglicherweise schon aus Foren oder ähnlichen Seiten kennt. Wenn man vornehmlich Texte schreiben und sich weniger mit der teilweise etwas sperrigen Syntax von HTML herumschlagen möchte ist Markdown eine ganz angenehme Alternative. Ich werde nicht im Detail auf die Formatierung mit Markdown eingehen, stattdessen werden wir uns im Anschluss möglichst schnell mit HTML befassen.

Um neue Einträge in dieser blogähnlichen Seite anzulegen, muss man einfach eine neue Datei im Ordner `_posts` erstellen. Achtet dabei darauf das Namensschema beizubehalten: `Jahr-Monat-Tag-Titel.md`.

<div class="task">
    <div class="header">Aufgabe: Der zweite Beitrag</div>
    <ul>
        <li>Erstellt einen zweiten Beitrag mit dem heutigen Datum und dem Titel "Von Markdown zu Jekyll". Der Text des Beitrages soll euren Vornamen (fettgedruckt), euren Nachnamen (kursiv) und eine Liste mit euren Hobbys beinhalten. Bei mindestens einem eurer Hobbies soll ein Link verwendet werden, der zu der entsprechenden Seite der Wikipedia führt.</li>
    </ul>
</div>
