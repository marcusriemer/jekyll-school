---
layout: post
author: Marcus
title: Planung einer Seite
description: >
  Alle technischen Kenntnisse über die Entwicklung von Webseiten bringen nichts, wenn man nichts zu sagen hat oder die Planung der Webseite zu sehr vernachlässigt. Dieser Beitrag soll euch eine kurze Gedächtnisstütze sein, welche Aspekte ihr bei der Planung eurer Webseite berücksichtigen solltet.
---
{{ page.description }} Die hier vorgestellte Reihenfolge ist dabei nicht die einzig mögliche, man kann sicherlich insbesondere die Punkte "Module" und "Navigation" auch austauschen.

## Die einzelnen Module

Abstrakt betrachtet lassen sich fast alle typischen Elemente von Webseiten in eine Kategorie einordnen. Webentwickler sprechen dann häufig von verschiedenen Modulen, aus denen eine Seite zusammengestellt ist. Diese Elemente kann man sich ein bisschen wie einen Werkzeugkasten vorstellen: Es handelt sich um Bestandteile, die auf verschiedenen Webseiten zu verschiedenen Zwecken zwar leicht variiert werden, in Ihrer Funktion aber weitgehend identisch bleiben.

* In einem *Nachrichtenbereich* geht es zunächst um einfache Textbeiträge, die chronologisch sortiert aufgeführt werden. Je nach Komplexität der Seite werden diese Texte von Metadaten begleitet: Dem Datum der Veröffentlichung, der Name eines Autors, ein Vorschaubild, eine Auflistung von "Tags" oder Kategorien ...
* Die meisten Webseiten geben dem Benutzer eine Möglichkeit an die Hand, bestimmte Personen zu erreichen. Das entsprechende *Kontaktmodul* ist je nach Anzahl der Personen die erreicht werden müssen auch sehr verschieden kompliziert. Bei einer privaten Homepage mag eine einfache Seite mit dem Hinweis "Sie finden mich auf Facebook und Twitter, das hier ist meine Email Adresse" noch ausreichend sein. Sofern man die entsprechende Seite aber z.B. für einen Verein erstellt, stehen häufig neben den zu erreichenden Personen auch noch deren Funktionen im Vordergrund.
* Ein *allgemeines Textmodul* kann auf viele unterschiedliche Arten und Weisen genutzt werden. Typischerweise erlauben allgemeine Textmodule einen hohen Grad an Verlinkung zwischen den einzelnen Webseiten und haben eine "Einstiegsseite", von der aus die Unterseiten erreicht werden. Diese Art von Modul findet man häufig auf Seiten die auf die ein oder andere Art Wissen sammeln oder vermitteln möchten, wie z.B. in der Wikipedia.
* Der Umgang strukturierte Umgang mit Mediendaten, speziell Fotos, wird häufig in Form einer *Galerie* bewerkstelligt. Die Anforderungen sind hier typischerweise relativ ähnlich zu denen im Nachrichtenbereich: Es muss geklärt werden, welche Metadaten ein Medienobjekt begleiten sollen. Außerdem werden Fotos typischerweise in Galerien oder Alben unterteilt, bei Videos ist eine solche Struktur seltener.
* *Strukturierte Auflistungen* von Personen, Vereinen, Material ... Häufig geht es auf Webseiten darum viele Inhalte möglichst gleichartig zu präsentieren. Ein typisches Beispiel dafür wären z.B. die Spieler eines Fußballvereins. Die einzelnen Seiten solcher Auflistungen bestehen oftmals aus keinen anderen Inhalten als den Daten im Frontmatter.

## Die Navigation

Nachdem man sich nun für eine Menge an benötigten Modulen entschieden hat stellt sich nun die Frage nach der allgemeinen Struktur der Seite. Welcher Inhalt soll unter welcher URL erreichbar sein? Was ist der erste Inhalt, den der Besucher zu Gesicht bekommt? Grundsätzlich bietet es sich an, die Struktur der Seite schonmal in eine Mindmap zu gießen. Die Mindmap als solche ist erst einmal ein relativ informelles Instrument, dass sich aber schon hervorragend eignet um die Navigation für die Seite einzugrenzen. Um die einzelnen Bereiche zu gewichten, lohnt es sich zu hinterfragen mit wie vielen "Klicks" der Benutzer einen bestimmten Inhalt erreichen können soll.

Ein oftmals vergessener Aspekt der Navigation ist, dass ein Modul natürlich in verschiedenen Kontexten genutzt werden kann. Wenn ein Fußballverein zum Beispiel eine Galeriemodul nutzen möchte um seinen Sportplatz und das Vereinsheim vorzustellen ist es nicht zwingend erforderlich dafür genau einen Menüpunkt "Galerie" vorzusehen. Stattdessen wären auch zwei Verweise wie "Unser Sportplatz" und "Unser Vereinsheim" denkbar.

## Die Metadaten

Nachdem nun alle verwendeten Module bekannt sind muss man sich Gedanken machen wie die einzelnen Inhalte dieser Module strukturiert werden können. Man sucht also nach festen YAML Schlüsseln, deren Existenz man für jeden Inhalt voraussetzen kann. Dieser Schritt ist in der Praxis schwierig, weil sich für 90% der Inhalte schnell gemeinsame Schlüssel ausmachen lassen, die restlichen 10% aber häufig "Ausreisser" darstellen, die sehr unglücklich aus dem Rahmen fallen. Dummerweise gibt es für diesen Schritt auch keine perfekte Herangehensweise. Es ist schlicht eine ganze Menge Erfahrung nötig, um die Datenmodellierung für die eigene Seite zu einem sauberen Ergebnis zu bringen. Um euch aber wenigstens ein klein bisschen Gespür für die zu erwartenden Probleme an die Hand zu geben, widmen wir uns einem Beispiel.

Der Fussballverein "Stiftung Wadentest" plant das Modul "Unser Team", in dem alle Mitglieder des Teams aufgeführt werden. Dabei sollen für jedes Teammitglied die Größe, das Gewicht, die Position, das Alter und der "starke" Fuß erfasst werden. Der Webentwickler sammelt diese Daten vom Team ein und beginnt mit der Erstellung der Webseite, die wenige Wochen später auch online geht. Kaum ist die Seite jedoch öffentlich einsehbar, kommt es schon zu den ersten Beschwerden: Der Spieler "Müller" hatte zwischenzeitlich Geburtstag und ist mittlerweile 16 Jahre alt, nicht mehr 15. Hier hat sich der Entwickler also dummerweise für eine unpassende Art der Erfragung der Daten entschieden: Schlauer wäre es gewesen statt des Alters den Geburtstag zu erfragen. Dann könnte man die Seite z.B. täglich neu erzeugen, für alle Spieler das Geburtsdatum einfach ausrechnen lassen und müsste nicht immer wieder von Hand einzelne Altersdaten von Spielern verändern.

Nun gehen ein paar weitere Wochen ins Land, die Saison ist mittlerweile beendet und es kommt zu einem Trainerwechsel. Der neue Trainer sieht die Webseite und ist empört, dass offensichtlich nur Spieler auf der Webseite erwähnt werden. Er wendet sich an den Webentwickler um auch auf der Seite aufgeführt zu werden und erhält den gleichen Fragebogen wie die Spieler. Man muss dem Entwickler zu gute halten, dass Ihm nicht bekannt sein konnte, dass der neue Trainer im Rollstuhl sitzt. Die Frage nach dem "starken Fuß" war unter diesen Umständen aber doppelt unpraktisch ... Leider ist die Lösung dieser Problematik weniger eindeutig: Fürs Erste würde es natürlich helfen, zwei verschiedene Arten von Teammitgliedern zu erfassen: Spieler und Trainer. Diese verschiedenen Arten von Personen haben dann natürlich auch unterschiedliche Eigenschaften: So könnte man bei den Spielern den "starken Fuß" ja weiterhin erfassen, bei dem Trainer stattdessen vielleicht die Stufe des Trainerscheins. Aber wo bleibt in dieser Logik zum Beispiel der Torwart? Spielt für Ihn der starke Fuß eine Rolle? Was ist mit Personen wie dem Platzwart, wo findet seine Einordnung statt? Alle diese Probleme lassen sich unmöglich im Voraus erahnen, daher ist eine aufmerksame Kommunikation mit dem Auftraggeber um so wichtiger. Er muss klar entscheiden, welche Personengruppen für Ihn existieren, in welchen Daten sich diese Gruppen unterscheiden und welche Personen überhaupt erwähnenswert sind.

Der Unterschied zwischen einem erfahrenen Webentwickler und einem Neuling ist, dass einem Erfahrung in der Datenmodellierung erlaubt die richtigen Fragen zu stellen. Die wenigsten Personen, die eine Webseite in Auftrag geben, haben ein gutes Gespür dafür welche Informationen auf der Seite dargestellt werden sollte. 

## Das Layout

Nachdem die Datenmodellierung nun abgeschlossen worden ist geht es ans Layout. Der erste Punkt der hier zu klären ist, ist die Gestaltung der Navivation. Gehört Sie an den linken oder rechten Rand? Oder sollte Sie "oben" an der Seite kleben? Ist die Seite klein genug um jede Unterseite in der Navigation aufzuführen oder braucht es eine Menüführung über mehrere Ebenen?

Die anderen Layoutentscheidungen orientieren sich stark an den Bedürfnissen der Komponenten: Wie groß sollen die Überschriften für Nachrichten sein? Gehört zu jeder Nachricht ein Bild, wo taucht das Datum und der Name des Autors auf? Gibt es "Hauptnachrichten" die immer etwas größer im Kopfbereich der Seite zu sehen sind?

## Das Design

Wenn das Design steht kann man damit anfangen, die Seite auch ein wenig besser aussehen zu lassen. Es ist nun an der Zeit sich für eine Farbpalette zu entscheiden, passende CSS Regeln auszuprobieren, mit Bildern zu experimentieren ...
