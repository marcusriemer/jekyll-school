---
title: Einrichtung von Jekyll
layout: post
author: Marcus
description: >
  Für die Webentwicklung in diesem Kurs benötigt ihr eine Reihe von Programmen, die in der FH Wedel schon vorinstalliert sind. Wenn Ihr auch von Zuhause aus arbeiten wollt findet Ihr hier die Links zu den verwendeten Programmen sowie eine kurze Anleitung zum Starten von Jekyll.
paths:
  jekyll-web: "http://www.fh-wedel.de/~mri/jekyll-2.5.2-portable"
---

{{page.description}}

Wir beginnen hier mit also mit dem Abschnitt "Euer erster eigener Beitrag", die beiden vorigen Abschnitte sollen euch zuhause daran erinnern, welche Programme ihr nochmal benötigt.

### Benötigte Programme

Um mit Jekyll zu arbeiten, müssen wir erstmal eine Reihe von Programmen organisieren.

* Jekyll selbst wird normalerweise unter Linux und Mac OS verwendet. Es gibt zwar verschiedene Anleitungen, welche die Einrichtung auch für Windows erläutern, das Vorgehen ist grundsätzlich aber wenig intuitiv, sofern man sich mit der Kommandozeile nicht schon auskennt … Zum Glück gibt es eine Alternative: Dieses Archiv [[7z]({{page.paths.jekyll-web}}.7z) oder [zip]({{page.paths.jekyll-web}}.zip)] von [Madhur Ahuja](http://www.madhur.co.in/) enthält eine vorkonfigurierte Jekyll Installation, die man “einfach so” benutzen kann. Wer mag kann sich den Ordner auf auch einen USB-Stick ziehen und dann von (fast) jedem Windows PC an seiner Seite basteln.

* Die von euch zu schreibenden Quelltexte könnte man zwar grundsätzlich auch mit dem Windows Notepad bearbeiten, ich empfehle stattdessen aber lieber [Notepad++](http://www.notepad-plus-plus.org/).

* Wenn ihr eure Webseite veröffentlichen möchte geschieht das in der Regel über (S)FTP. Für Windows gibt es mit  [WinSCP](http://winscp.net/eng/index.php) ein sehr gutes Programm um sich zu den meisten Servern zu verbinden.


<div class="warn">
Das <code>.7z</code> Archiv ist mit etwa 700 MB merklich kleiner als das 1.2 GB <code>.zip</code> Archiv. Solltet Ihr keinen entsprechenden Entpacker installiert benutzt einfach <a href="http://www.7-zip.org/">7- Zip</a> Davon gibt es auch eine portable Version, die man verwenden könnte, sofern am entsprechenden PC nicht die Möglichkeit besteht neue Programme zu installieren.
</div>

### Einrichtung von Jekyll

Ich gehe davon aus, dass ihr grundsätzlich in der Lage seid ein Programm mit einem "normalen" Installer wie dem von Notepad++ oder WinSCP zu installieren. Jekyll selber wird aber ein wenig anders eingerichtet. Streng genommen ist Jekyll auch kein einzelnes Programm, sondern eine ganze Sammlung verschiedener Programme die gemeinsam in der Lage sind eine Webseite zu erstellen.

Entpackt bitte das oben erwähnte Zip Archiv an einem Ort eurer Wahl. Das Archiv enthält neben einer Reihe von Unterordnern eine für euch wesentliche Datei: die `develop.cmd`. Diese Datei sorgt dafür, dass euer System weiss wo sich die einzelnen Bestandteile von Jekyll befinden und startet dann eine Konsole.

<div class="task">
    <div class="header">Aufgabe: Der erste Start von Jekyll</div>
    <p>
      Startet die <code>develop.cmd</code> Datei und gebt in der erscheinenden Konsole folgendes ein: <code>jekyll --version</code>. Sofern Ihr bei der Installation nichts verkehrt gemacht habt, sollte die Ausgabe so ähnlich aussehen wie in dem Screenshot. Solltet Ihr auf Probleme stoßen, kontaktiert mich gerne über <a href="http://www.fh-wedel.de/mitarbeiter/mri">meine berufliche Mail</a>.
    </p>
    <img src="{{ site.baseurl }}/assets/cmd-jekyll-version.png">
</div>
